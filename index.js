const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');
const { prefix, token } = config;
const fs = require('fs');
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

client.on('ready', () => {
    // Logged on console start
    console.log(`Logged in as ${client.user.tag}`);
});

client.on('message', msg => {
    if(!msg.content.startsWith(prefix)) {
        return;
    }
});

client.login(token);